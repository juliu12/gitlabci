---
title: Hello Docusauro.io
author: Juliu Chagas
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum dignissim ultricies. Fusce rhoncus ipsum tempor eros aliquam consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. 

<!--truncate-->

## Agora Vai! 

Vivamus elementum massa eget nulla aliquet sagittis. Proin odio tortor, vulputate ut odio in, ultrices ultricies augue. Cras ornare ultrices lorem malesuada iaculis. Etiam sit amet libero tempor, pulvinar mauris sed, sollicitudin sapien.

```
drwxrwxr-x   8 julio julio 4,0K jan  3 16:26 .
drwxrwxr-x   4 julio julio 4,0K jan  3 16:18 ..
drwxrwxr-x   2 julio julio 4,0K jan  6 16:50 blog
drwxrwxr-x   2 julio julio 4,0K jan  3 16:18 core
drwxrwxr-x   2 julio julio 4,0K jan  3 16:26 i18n
drwxrwxr-x 718 julio julio  24K jan  3 16:18 node_modules
-rw-rw-r--   1 julio julio  376 jan  3 16:18 package.json
-rw-rw-r--   1 julio julio 367K jan  3 16:18 package-lock.json
drwxrwxr-x   3 julio julio 4,0K jan  3 16:18 pages
-rw-rw-r--   1 julio julio 4,0K jan  3 16:18 README.md
-rw-rw-r--   1 julio julio  190 jan  6 16:33 sidebars.json
-rw-rw-r--   1 julio julio 3,2K jan  6 16:13 siteConfig.js
drwxrwxr-x   4 julio julio 4,0K jan  3 16:18 static

```

